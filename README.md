# CMake Tutorial

A tutorial made by me when I was learning cmake.


## how to build

0. export BOOST_ROOT=/usr/local/lib/boost_1_53_0
  1. or you could add it to ~/.bashrc
1. mkdir build 
2. cd build
3. cmake ..
4. make

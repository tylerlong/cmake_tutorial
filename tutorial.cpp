#include <iostream>

#include <boost/filesystem.hpp>

using namespace std;
namespace fs = boost::filesystem;

void file_list(const string& path)
{
    fs::directory_iterator end_itr;
    for (fs::directory_iterator itr(path); itr != end_itr; itr++)
    {
        string path = itr->path().generic_string();
        cout << path << endl;
    }
}

int main (int argc, char *argv[])
{
  if (argc < 2)
  {
    cout << "Usage: " << argv[0] << " /path/to/file_or_folder" << endl;
    return 1;
  }
  
  string path(argv[1]);  
  
  file_list(path);
}
